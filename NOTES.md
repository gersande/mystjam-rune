**Notes on this game** 

Tentative Title: Rune

Inventory
* -> Notebook for keeping and updating (how do I implement that in Twine? Javascript -- >  http://www.dreamincode.net/forums/topic/89001-simple-notepad/ or http://stackoverflow.com/questions/25916904/html-and-javascript-how-to-save-plain-text-right ) 

Characters
* "The Stranger" : the unnamed, undefined protagonist. Unlike most Myst games, I will either allow the player to name the characters or chose the pronouns (I may default to gender neutral). 

Twine 2 versus Twine 1 - I like the interface of Twine 2 but there seem to be a serious lack of the features that have been built up around Twine 1. :/ 

HOW TO "RETURN" TO A PREVIOUS PASSAGE without losing progress, actually grabbed the code from http://twinery.org/forum/discussion/5096/change-undo-to-go-back-in-harlowe 

**Log**

* January 9 2016 - spent the day researching for the backstory, thinking about the plot and have Lindar who has offered to help with music! :D
* January 10 2016 - this is the day I'll have most time to work on the game, so I need to get as much done as possible. 
* January 11 2016 - it's like 2 am but I made progress on a thing