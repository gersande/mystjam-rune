**About** 

This game is made for the Myst jam http://itch.io/jam/myst-jam January 8 to January 17th 6:59 PM.  

**Files**

* README.md
* NOTES.md &bull; *game development notes*
* test &bull; *folder for javascript testing*
* assets &bull; *folder for assets* 
* mystjam-rune.html &bull; *html file created by twine 2 gui*

**Notes**

[Read the game development notes here.](https://gitlab.com/gersande/mystjam-rune/blob/90312dec96b57ea0295349df4a18de2300f2f57c/NOTES.md) **Contains spoilers for my own game and the Myst series.**

**Resources:**

* [D'ni numerals](http://dni.wikia.com/wiki/D%27ni_Numerals)
* [Notes on Ages](http://dni.wikia.com/wiki/Ages) & [D'ni cosmology from the Wiki](http://dni.wikia.com/wiki/Great_Tree_of_Possibilities) 
